import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Authentication/LoginTest'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/a_Employees'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/h3_Employees'), 'Employees')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Create Employee'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_First Name_first_name'), 
    'Frank')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Last Name_last_name'), 
    'Idowu')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Work Email_email'), 'frankido@yopmail.com')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Role_role'), 'Ganitroir')

WebUI.scrollToElement(findTestObject('Page_Equip your remote hires with work tools/select_employee_type_dropdown'), 0)

WebUI.click(findTestObject('Page_Equip your remote hires with work tools/select_employee_type_dropdown'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Full time'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Continue'))

WebUI.click(findTestObject('Page_Equip your remote hires with work tools/select_flag'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/span_United Kingdom'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Phone_object Object'), 
    '+44 7121 928818')

WebUI.click(findTestObject('Page_Equip your remote hires with work tools/select_country'))

WebUI.click(findTestObject('Page_Equip your remote hires with work tools/div_Nigeria'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_State  Province_react-select-5-input'), 
    'la')

WebUI.click(findTestObject('Page_Equip your remote hires with work tools/div_Lagos'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_City_city'), 'Ikorodu')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Address_address'), '14 Osholigbe')

WebUI.sendKeys(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Address_address'), Keys.chord(
        Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Address_address'), '14 Osholigbeyin Street, Ikorodu, Nigeria')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Save Info'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Employee Created Successfully'), 
    'Employee Created Successfully')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Create Employee'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_First Name_first_name'), 
    'Oluwasegun')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Last Name_last_name'), 
    'Talabi')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Work Email_email'), 'talabioluwasegun@yopmail')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Role_role'), 'CCOP')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Select_CountryCode'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Full time_1'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Work Email_email'), 'talabioluwasegun@yopmail.com')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Continue'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Phone_object Object'), 
    '+234 199 211 020 011')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Select_CountryCode'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Kenya'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Select_CountryCode'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Eastern'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_City_city'), 'Adreal')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Address_address'), '12 Street Di')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/span_12th Street, Astoria, NY, USA'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Save Info'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/path'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Yes'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Successfully deleted employee'), 
    'Successfully deleted employee')

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_Employees_search'), 'Joy')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/span_Joy Ajayi'), 
    'Joy Ajayi')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/svg'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Edit'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Select_CountryCode'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/div_Freelance'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Save Changes'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Edit'))

WebUI.setText(findTestObject('Object Repository/Page_Equip your remote hires with work tools/input_City_city'), 'Onipanu')

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/button_Save Changes'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/path_1'))

WebUI.click(findTestObject('Object Repository/Page_Equip your remote hires with work tools/a_Employees'))

WebUI.callTestCase(findTestCase('Authentication/LogoutTest'), [:], FailureHandling.STOP_ON_FAILURE)

