<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_City_city</name>
   <tag></tag>
   <elementGuidId>03230149-fb29-44bc-8709-684eb8e09b95</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='city']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#city</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4f7aca03-769a-48c6-a6ff-8cba034eec22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>f3705c59-c33e-43cc-90dd-d6daf62bbc3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>city</value>
      <webElementGuid>9be73bb2-86e1-4023-be77-9288d792359d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>city</value>
      <webElementGuid>54289417-ad1b-4da0-bd54-ef74c37f7efb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>field</value>
      <webElementGuid>1c3d0975-307f-48eb-a93a-82120afd6dd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter city</value>
      <webElementGuid>95e08dda-b6a2-45ec-825a-4f4e28a2a7c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;city&quot;)</value>
      <webElementGuid>7dcc2412-242d-49bb-bd3b-5f15557cc9d5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='city']</value>
      <webElementGuid>cbd11e0e-9ee2-4f67-9c13-b0c08db7c2eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/main/div/main/div/form/div/div[4]/div/input</value>
      <webElementGuid>c4ddfa2b-de52-4ba4-8f3a-0a1b920f705e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/input</value>
      <webElementGuid>514ada4a-59be-4a4b-8899-061e229a7c15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'city' and @id = 'city' and @placeholder = 'Enter city']</value>
      <webElementGuid>6a38f5fc-d0c0-4f75-b72e-4346318cd85f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
