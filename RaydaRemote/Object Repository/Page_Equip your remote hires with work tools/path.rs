<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>85907bf2-0650-4ef4-9ecd-ae8cd8071d0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>tr:nth-of-type(10) > td:nth-of-type(5) > div > div:nth-of-type(2) > svg > g > path:nth-of-type(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>b275f2b7-a565-4836-bac0-359f9eba4f94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M17 6h5v2h-2v13a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8H2V6h5V3a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v3zm1 2H6v12h12V8zm-9 3h2v6H9v-6zm4 0h2v6h-2v-6zM9 4v2h6V4H9z</value>
      <webElementGuid>0585a561-2e95-4e9c-845a-772b3befebeb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[2]/main[1]/div[@class=&quot;sc-lknQiW nRjBw&quot;]/div[@class=&quot;sc-eVspGN eYecMK&quot;]/section[@class=&quot;table_section&quot;]/table[@class=&quot;sc-jfTVlA eCyQJC&quot;]/tbody[1]/tr[10]/td[5]/div[1]/div[2]/svg[1]/g[1]/path[2]</value>
      <webElementGuid>9beb2e11-2425-49ad-8284-34e047316bb2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
