<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Joy Ajayi</name>
   <tag></tag>
   <elementGuidId>b4b356c4-f8a9-4812-b7fc-834d54eb7985</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/main/div/div[2]/section[2]/table/tbody/tr/td/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td > div > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d770d02d-c409-4c55-9407-8c1c6350136c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Joy Ajayi</value>
      <webElementGuid>be89811d-b2b1-41d4-8a66-f53660e15f4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[2]/main[1]/div[@class=&quot;sc-lknQiW nRjBw&quot;]/div[@class=&quot;sc-eVspGN eYecMK&quot;]/section[@class=&quot;table_section&quot;]/table[@class=&quot;sc-jfTVlA eCyQJC&quot;]/tbody[1]/tr[1]/td[1]/div[1]/span[1]</value>
      <webElementGuid>35bbdf0d-4e3e-407d-bb49-23f4fec48270</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/main/div/div[2]/section[2]/table/tbody/tr/td/div/span</value>
      <webElementGuid>636d967b-619a-48a2-a926-5e433822ab31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='J A'])[1]/following::span[1]</value>
      <webElementGuid>2ad8906f-d335-45ff-b335-5e639759f687</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/following::span[1]</value>
      <webElementGuid>8c40c73b-5cf6-4be2-9496-1a59cb678e9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Full time'])[1]/preceding::span[1]</value>
      <webElementGuid>913c85b9-b5ba-4499-b200-7e75fe96044a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Relations'])[1]/preceding::span[1]</value>
      <webElementGuid>7e3a2c21-92f8-46d0-acae-8ca5311fbd6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Joy']/parent::*</value>
      <webElementGuid>35c54558-88b3-4c57-aae4-cfb47df0d624</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/span</value>
      <webElementGuid>51b6928c-3a29-4b3d-884f-06597a78294a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Joy Ajayi' or . = 'Joy Ajayi')]</value>
      <webElementGuid>ea6280e9-86a6-44c1-8dea-5f9aa7078353</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
