<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_12th Street, Astoria, NY, USA</name>
   <tag></tag>
   <elementGuidId>ba22ad92-4d80-41cb-adfb-657392f6dc19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id='PlacesAutocomplete__suggestion-Eh0xMnRoIFN0cmVldCwgQXN0b3JpYSwgTlksIFVTQSIuKiwKFAoSCYmvWWLLWMKJEUvd4nfWcIwAEhQKEglTpbXnNl_CiREOEYSsxK8KnA']/span)[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e16bf06f-4ebd-4a9d-9bbf-3794d7c5dc04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>12th Street, Astoria, NY, USA</value>
      <webElementGuid>f6fd0d79-e326-4abe-83ea-7eb1ddcfb1ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[2]/main[1]/div[@class=&quot;sc-dWRHGJ fSsPaZ&quot;]/main[1]/div[@class=&quot;sc-eGJbfJ lORYa&quot;]/form[1]/div[@class=&quot;form_container&quot;]/div[@class=&quot;sc-bYMpWt iaWqSe&quot;]/div[@class=&quot;input_container&quot;]/div[@class=&quot;autocomplete-dropdown-container&quot;]/div[@id=&quot;PlacesAutocomplete__suggestion-Eh0xMnRoIFN0cmVldCwgQXN0b3JpYSwgTlksIFVTQSIuKiwKFAoSCYmvWWLLWMKJEUvd4nfWcIwAEhQKEglTpbXnNl_CiREOEYSsxK8KnA&quot;]/span[1]</value>
      <webElementGuid>be2d1c9e-a747-44eb-94f8-330cc735d7b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//div[@id='PlacesAutocomplete__suggestion-Eh0xMnRoIFN0cmVldCwgQXN0b3JpYSwgTlksIFVTQSIuKiwKFAoSCYmvWWLLWMKJEUvd4nfWcIwAEhQKEglTpbXnNl_CiREOEYSsxK8KnA']/span)[3]</value>
      <webElementGuid>35d6e99f-4ca0-4622-a1f6-87b7a8858a9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dirty Dough North Ogden, 12th Street, Ogden, UT, USA'])[1]/following::span[1]</value>
      <webElementGuid>ca0ff076-ee72-425f-98a2-bb0c6f4d9fc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Address'])[1]/following::span[3]</value>
      <webElementGuid>25fbb1fd-6503-4a2b-a8a4-1a34c328161e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='← Back'])[1]/preceding::span[3]</value>
      <webElementGuid>2e8aa182-d2ee-4c44-becd-3a1205e48ac7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save Info'])[1]/preceding::span[3]</value>
      <webElementGuid>3092b699-6506-4295-830c-66fcef70c2ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='12th Street, Astoria, NY, USA']/parent::*</value>
      <webElementGuid>e86c2767-0e19-4fc5-abe8-99606b8a8d9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/span</value>
      <webElementGuid>e07fbea0-79b7-4076-b6e9-31fb5b196da6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '12th Street, Astoria, NY, USA' or . = '12th Street, Astoria, NY, USA')]</value>
      <webElementGuid>17f39ad6-0195-4dc1-a1f7-e7c0f31063e6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
