<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_United Kingdom</name>
   <tag></tag>
   <elementGuidId>87592e64-5a88-4699-92ad-13081534ce6a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.country.preferred > span.country-name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/main/div/main/div/form/div/div/div/div[2]/ul/li[201]/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[@class = 'country-name' and (text() = 'United Kingdom' or . = 'United Kingdom')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a7b38206-26f7-414a-8d71-9e16f3fa9ace</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>country-name</value>
      <webElementGuid>fae07bfb-6fed-47e1-9260-0c86144d3fd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>United Kingdom</value>
      <webElementGuid>1e42f86a-39b0-4b52-898a-2ac9baec503f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[2]/main[1]/div[@class=&quot;sc-dWRHGJ fSsPaZ&quot;]/main[1]/div[@class=&quot;sc-eGJbfJ lORYa&quot;]/form[1]/div[@class=&quot;form_container&quot;]/div[@class=&quot;sc-kMjNwy gaaAnJ&quot;]/div[@class=&quot;react-tel-input&quot;]/div[@class=&quot;flag-dropdown open&quot;]/ul[@class=&quot;country-list&quot;]/li[@class=&quot;country preferred&quot;]/span[@class=&quot;country-name&quot;]</value>
      <webElementGuid>357f7537-e230-449d-bba4-158f63902b0e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/main/div/main/div/form/div/div/div/div[2]/ul/li[201]/span</value>
      <webElementGuid>90851c01-530f-47cd-bab0-e77db0362ec4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='United Arab Emirates'])[1]/following::span[2]</value>
      <webElementGuid>35e7923a-cd08-443d-a781-45456473ac79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ukraine'])[1]/following::span[4]</value>
      <webElementGuid>4b61d1bd-b6ca-4e1c-9164-001678c0a32c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='United States'])[1]/preceding::span[2]</value>
      <webElementGuid>97522426-f449-453c-aac7-5959133edc35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Uruguay'])[1]/preceding::span[4]</value>
      <webElementGuid>b7a1b0db-d6ab-444e-a0a7-0fdeae7e2c35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='United Kingdom']/parent::*</value>
      <webElementGuid>f4e398ca-427e-4802-81b1-28b979101610</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[201]/span</value>
      <webElementGuid>f3564e9a-d962-4536-a242-ca3937151dde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'United Kingdom' or . = 'United Kingdom')]</value>
      <webElementGuid>f99e0812-5ef3-47af-8902-1225a06f0531</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
