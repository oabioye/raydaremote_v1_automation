<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Phone_object Object</name>
   <tag></tag>
   <elementGuidId>c4649ee7-d64b-4dae-ac91-709a60481577</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='[object Object]']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;[object Object]&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a50377b5-620f-4c7f-b65b-a3371f0d813b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>d785b84b-0788-4ec8-af82-b3f7c9daba08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>1 (702) 123-4567</value>
      <webElementGuid>2c404078-8171-4608-8208-29cb55733ee3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>9faea90e-2e4a-49c3-9f7c-c53413337ca1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>[object Object]</value>
      <webElementGuid>14cffde5-9f8d-4f14-b76a-003db66d40ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>country</name>
      <type>Main</type>
      <value>ng</value>
      <webElementGuid>f62054e8-1b45-49d7-8535-3e42023a9a1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>+44 7</value>
      <webElementGuid>98a38ddd-10de-457e-bc45-8edfe4542401</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[2]/main[1]/div[@class=&quot;sc-dWRHGJ fSsPaZ&quot;]/main[1]/div[@class=&quot;sc-eGJbfJ lORYa&quot;]/form[1]/div[@class=&quot;form_container&quot;]/div[@class=&quot;sc-kMjNwy gaaAnJ&quot;]/div[@class=&quot;react-tel-input&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>dbf20c4c-740f-4780-9795-29e0959dc301</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='[object Object]']</value>
      <webElementGuid>9a4330eb-b28d-4e35-a8f2-da6dbd8c50a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/main/div/main/div/form/div/div/div/input</value>
      <webElementGuid>25b3bcda-a549-4422-86ed-971c22697080</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>5ea6e538-c5cd-4e16-a931-79f41351e88d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = '1 (702) 123-4567' and @type = 'tel' and @name = '[object Object]']</value>
      <webElementGuid>93e7611e-bcea-40f8-9e35-8101a85e0fc8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
