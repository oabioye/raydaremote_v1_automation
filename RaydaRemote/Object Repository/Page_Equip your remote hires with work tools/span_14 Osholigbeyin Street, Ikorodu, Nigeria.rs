<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_14 Osholigbeyin Street, Ikorodu, Nigeria</name>
   <tag></tag>
   <elementGuidId>ff4b238b-ead1-4ae6-9af1-2f60455dcd4c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='PlacesAutocomplete__suggestion-EigxNCBPc2hvbGlnYmV5aW4gU3RyZWV0LCBJa29yb2R1LCBOaWdlcmlhIjASLgoUChIJ8Ue6Wu3uOxARbk23oqeSPD8QDioUChIJ9RMwn_LuOxAR2M6URX5IRPM']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#PlacesAutocomplete__suggestion-EigxNCBPc2hvbGlnYmV5aW4gU3RyZWV0LCBJa29yb2R1LCBOaWdlcmlhIjASLgoUChIJ8Ue6Wu3uOxARbk23oqeSPD8QDioUChIJ9RMwn_LuOxAR2M6URX5IRPM > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4594b467-4145-442e-b8af-c820ab3b9e73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>14 Osholigbeyin Street, Ikorodu, Nigeria</value>
      <webElementGuid>1f32af19-dd23-477f-889d-f7f582cd89e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PlacesAutocomplete__suggestion-EigxNCBPc2hvbGlnYmV5aW4gU3RyZWV0LCBJa29yb2R1LCBOaWdlcmlhIjASLgoUChIJ8Ue6Wu3uOxARbk23oqeSPD8QDioUChIJ9RMwn_LuOxAR2M6URX5IRPM&quot;)/span[1]</value>
      <webElementGuid>7a4b1f91-59cc-4471-bd46-16ebcb278586</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='PlacesAutocomplete__suggestion-EigxNCBPc2hvbGlnYmV5aW4gU3RyZWV0LCBJa29yb2R1LCBOaWdlcmlhIjASLgoUChIJ8Ue6Wu3uOxARbk23oqeSPD8QDioUChIJ9RMwn_LuOxAR2M6URX5IRPM']/span</value>
      <webElementGuid>77683bf6-38e1-4099-8365-fd8d34deae09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Address'])[1]/following::span[1]</value>
      <webElementGuid>09ab0c1f-c507-4f0c-bc58-e0afa40efde6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/following::span[1]</value>
      <webElementGuid>cac3eb59-1f69-41b9-9010-c965e01c8410</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='← Back'])[1]/preceding::span[1]</value>
      <webElementGuid>d95a6df3-1ee8-4c8b-b736-04eafcbecce4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save Info'])[1]/preceding::span[1]</value>
      <webElementGuid>a55a6b32-8193-4f0e-8a20-a0ec80a62a44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='14 Osholigbeyin Street, Ikorodu, Nigeria']/parent::*</value>
      <webElementGuid>8a0dfd94-6bdd-4bd5-929e-958ddf45fac8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/span</value>
      <webElementGuid>ad35d50d-29b1-47f6-9d2d-2fa3a2c3ed78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '14 Osholigbeyin Street, Ikorodu, Nigeria' or . = '14 Osholigbeyin Street, Ikorodu, Nigeria')]</value>
      <webElementGuid>a86cb9a9-bdb8-4b14-ab54-c26c8a808b75</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
