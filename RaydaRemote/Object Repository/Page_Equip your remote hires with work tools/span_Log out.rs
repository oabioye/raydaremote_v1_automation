<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Log out</name>
   <tag></tag>
   <elementGuidId>b73d82ed-8a0a-4c10-a2e2-12dbd7ac8299</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div/div/header/div/div/div[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.profile-dropdown__actions > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>069536f3-d01e-4746-98ee-b32a3034456b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Log out</value>
      <webElementGuid>3aea0548-602e-4295-93df-0d8d6e5d9085</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[2]/div[@class=&quot;sc-lbVpMG cFlfsW&quot;]/div[@class=&quot;sticky-outer-wrapper&quot;]/div[@class=&quot;sticky-inner-wrapper&quot;]/header[@class=&quot;unSticky undefined&quot;]/div[@class=&quot;sc-gswNZR iHAOLv&quot;]/div[@class=&quot;profile-dropdown&quot;]/div[@class=&quot;profile-dropdown__actions&quot;]/span[1]</value>
      <webElementGuid>af4048ab-aa7e-486e-bafc-0abad758e5c1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/header/div/div/div[2]/span</value>
      <webElementGuid>14865448-d9a0-4ec0-abf1-15c327d5e917</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='saldim@yopmail.com'])[1]/following::span[1]</value>
      <webElementGuid>7c5b595a-bbdb-489d-8293-fe6319cd8e7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Salami Dimeji'])[1]/following::span[2]</value>
      <webElementGuid>ee5714e8-2c3f-4f1e-99fd-060cf95a78a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketplace'])[2]/preceding::span[1]</value>
      <webElementGuid>84ecb28d-9899-4071-8afb-5711d69b26b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='>'])[1]/preceding::span[1]</value>
      <webElementGuid>6884af97-a4d9-432d-8973-8fdeb03c1d0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Log out']/parent::*</value>
      <webElementGuid>8272f481-6420-4a28-a62a-304ff09d7104</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span</value>
      <webElementGuid>8f4b7d6b-6ebe-4ee7-ae3f-0ef9d6fa093c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Log out' or . = 'Log out')]</value>
      <webElementGuid>e48fe537-01bb-4b9d-938c-0d788ab088a0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
